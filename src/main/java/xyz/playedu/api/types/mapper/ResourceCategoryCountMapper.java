/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.types.mapper;

import lombok.Data;

/**
 * @Author 杭州白书科技有限公司
 *
 * @create 2023/3/15 11:11
 */
@Data
public class ResourceCategoryCountMapper {

    private Integer cid;

    private Integer total;
}
