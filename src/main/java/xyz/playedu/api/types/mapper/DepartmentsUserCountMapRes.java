/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.types.mapper;

import lombok.Data;

/**
 * @Author 杭州白书科技有限公司
 *
 * @create 2023/4/12 13:46
 */
@Data
public class DepartmentsUserCountMapRes {
    private Integer depId;

    private Integer total;
}
