/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.service;

import com.baomidou.mybatisplus.extension.service.IService;

import xyz.playedu.api.domain.UserUploadImageLog;

/**
 * @author tengteng
 * @description 针对表【user_upload_image_logs】的数据库操作Service
 * @createDate 2023-03-24 14:32:48
 */
public interface UserUploadImageLogService extends IService<UserUploadImageLog> {}
